
/**
 * @mixin
 */
export default {
  name: 'app-data',

  props: {
    /**
     * @param {string} url - Fetch data with given url
     */
    url: {
      type: String,
      required: true,
    },

    /**
     * @param {string} method - Fetch data with given Method
     * 'GET/POST/PUT/DELETE'
     */
    method: {
      type: String,
      required: true,
      validator: value => ['GET', 'POST', 'DELETE', 'PUT'].indexOf(value) !== -1,
    },

    /**
     * @param {object} params - Fetch data with given parameters
     */
    params: {
      type: Object,
      default: () => ({}),
    },

    /**
     * @param {string} filter_key - Data will be filtered by this key
     */
    filter_key: {
      type: String,
      default: '',
    },

    /**
     * @param {string|number} filter_value - Filter data by value of filter_key
     */
    filter_value: {
      type: [String, Number],
      default: '',
    },

    /**
     * @param {string|number} count - count for the API calls
     */
    fetch_count: {
      type: [String, Number],
      default: 1,
      validator: val => val > 0,
    },

    /**
     * @param {string} sort_key - This key will be used in each object to sort data
     */
    sort_key: {
      type: String,
      required: true,
    },
  },

  data() {
    return {
      /**
       * @type {boolean} - is loading are in progress now
       */
      is_loading: false,

      /**
       * @type {boolean} - is loading are finished
       */
      loaded: false,

      /**
       * @type {boolean} - is API has more data
       */
      appup_has_more: false,

      /**
       * @type {number} - current API calls cursor.
       * Increments with every API call
       */
      appup_cursor: 1,

      /**
       * @type {array} - data from API to sort, filter, etc.
       */
      data: [],

      /**
       * @type {array} - original data from API
       */
      original_data: [],
    }
  },

  methods: {
    /**
     * @function @async set_ajax - Get data from API with url, method and params from props
     * @public
     * and sets it to data and original_data. If response has no data - sets empty array to data
     * Also updates url depending on appup_cursor and response length
     */
    async set_ajax() {
      const data = await this.fetch_data(`${this.url}${this.appup_cursor}`, this.method, this.params)
      const search = `count=${this.appup_cursor}&offset=${data.length}`
      window.history.pushState(null, null, search)
      this.appup_cursor += 1

      this.original_data = [...data]
      this.data = data || []
      this.has_more()
    },

    /**
     * @function has_more - Check if there is more data to fetch
     * @public
     */
    has_more() {
      this.appup_has_more = (this.data && this.data.length) < this.fetch_count
    },

    /**
     * @function @async fetch_data - local function to get data from API
     * @public
     * @param {string} url - url to get data from
     * @param {string} method - name of method
     * @param {string} params - url to get data from
     */

    async fetch_data(url, method, params) {
      let response = null
      // handle loader variables
      this.loaded = false
      this.is_loading = true

      /**
       * Fires before call to API starts.
       * @event loading
       * @type {string}
       */
      this.$emit('loading')

      response = await fetch(url, { ...params, method })

      this.loaded = true
      this.is_loading = false

      /**
       * Fires when API gives responce.
       *
       * @event loaded
       * @type {string}
       * @property {Promise} response - response from API.
       */
      this.$emit('loaded', response)

      if (response.ok) {
        response = await response.json()

        /**
         * Fires when data is successfully loaded from API.
         *
         * @event success
         * @type {string}
         * @property {array} response - data from API.
         */
        this.$emit('success', response)
        return response
      }

      /**
       * Fires when data is failed to loaded from API.
       *
       * @event failure
       * @type {string}
       * @property {Promise} response - promise.
       */
      this.$emit('failure', response)
      return null
    },

    /**
     * @function get_cursor - get value of current appup_cursor
     * @returns {number|string}
     * @public
     */
    get_cursor() {
      return this.appup_cursor
    },

    /**
     * @function set_cursor - sets value of current appup_cursor
     * @type {number|string} num - new appup_cursor value
     * @public
     */
    set_cursor(num) {
      const cursor = parseInt(num, 10)
      if (num > 0) {
        this.appup_cursor = cursor
      }
    },

    /**
     * @function reset - resets data to original_data
     * @public
     */
    reset() {
      this.data = [...this.original_data]
    },

    /**
     * @function set_data - set new data
     * @param {array} new_data - new data to be used as data
     * @public
     */
    set_data(new_data = []) {
      if (!new_data.length) return
      this.original_data = [...new_data]
      this.data = new_data
    },

    /**
     * @function add - add new data to data
     * @param {array} new_data - new data to be added to data
     * @public
     */
    add(new_data = []) {
      if (!new_data.length) return
      this.original_data = [...this.data, ...new_data]
      this.data = [...this.data, ...new_data]
    },

    /**
     * @function sort - sort data with key specified in prop sort_key
     * @public
     */
    sort() {
      if (!this.sort_key) return

      this.data.sort((a, b) => {
        if (a[this.sort_key] > b[this.sort_key]) return 1

        if (a[this.sort_key] < b[this.sort_key]) return -1

        return 0
      })
    },

    /**
     * @function filter - filter data
     * @public
     * @param {string|number} key - filter data by this key
     * @param {string|number} key_value - filter data with this value in key
     */
    filter(key, key_value) {
      if (!key || !key_value) {
        key = this.filter_key
        key_value = this.filter_value
      }

      this.data = this.original_data
        .filter((obj) => {
          const obj_key = String(obj[key]).toLowerCase()
          const val = String(key_value).toLowerCase()

          return obj_key === val
        })
    },

    /**
     * @function remove - remove item from data
     * @param {string} key - filter data by this key
     * @param {string} key_value - filter data with this value in key
     * @public
     * Does not changes original_data
     */
    remove(key, key_value) {
      if (!key || !key_value) return

      this.data = this.data.filter(obj => obj[key] !== key_value)
    },
  },
}
