import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate'
import { Picker } from 'emoji-mart-vue'
import VueMoment from 'vue-moment'
import { Carousel } from 'bootstrap-vue/es/components'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'

Vue.use(BootstrapVue)
Vue.use(VeeValidate)
Vue.use(VueMoment)

// Bootstrap components
Vue.component('ac-image-slider', Carousel)

// Third party components direct registration
Vue.component('ac-emoji', Picker)

// Import components from Appup Components and register components globally
import * as Appup from '../components';
for (let compKey in Appup) {
  Vue.component(Appup[compKey].name, Appup[compKey])
}

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
