# ac-components

Examples in folder src

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Examples

Create a component usage examples in examples directory with file name as component name eg: ac-mentions.vue, after application is served (yarn serve), navigate to #/examples/{component name} eg: #/examples/ac-mentions to view results.


