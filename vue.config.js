/* Needed for v-runtime-template
 *https://github.com/alexjoverm/v-runtime-template#getting-started
 */

module.exports = {
  runtimeCompiler: true,
}
